# Cocky Chickens feat. Fresh Animations

Cocky Chickens feat. Fresh Animations is a fork of [Cluckier Chickens](https://www.planetminecraft.com/texture-pack/cluckier-chickens/) by creepermax123, and [Fresh Animations](https://modrinth.com/resourcepack/fresh-animations) by FreshLX

Unlike Cluckier Chickens, it makes the chickens look more vanilla.


## Dependencies
Requires [Entity Model Features](https://modrinth.com/mod/entity-model-features) and [Entity Texture Features](https://modrinth.com/mod/entitytexturefeatures)